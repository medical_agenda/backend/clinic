package com.clinic.person.service;

import com.clinic.person.module.Person;
import com.clinic.person.module.PersonDTO;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
@Transactional	
@ApplicationScoped
public class GestionPersonRest {
	@PersistenceContext
	EntityManager em;

	public void save(PersonDTO personDTO) {
		em.persist(builder(personDTO));
	}
	
	public Person search(Long ipPerson) {
		return em.find(Person.class, ipPerson);
	}
	//@TransactionAttribute
	public Boolean update(PersonDTO personDTO) {
		Person person = builder(personDTO);
		if (person!= null) {
			em.merge(person);
			return true;
		}
		return false;
	}
	
	public Boolean delete(Long ipPerson) {
		Person person =search(ipPerson);
		if (person!= null) {
			em.remove(person);
			return true;
		}
		return false;
	}
	
	private Person builder(PersonDTO personDTO) {
		Person person = new Person();
		person.setId(personDTO.getId());//Creo que este no va, porque se crea solo en la BD
		person.setIdentificationNumber(personDTO.getIdentificationNumber());
		person.setFirstName(personDTO.getFirstName());
		person.setSecondName(personDTO.getSecondName()); 
		person.setFistLastName(personDTO.getFistLastName());
		person.setSecondLastName(personDTO.getSecondLastName());
		person.setPhoneNumber(personDTO.getPhoneNumber());
	//	person.setPersonTypeEnum(personDTO.getPersonTypeEnum());
		person.setDTypId(personDTO.getDTypId());
		person.setEmail(personDTO.getEmail());
		person.setBirthDate(personDTO.getBirthDate());
		person.setUserName(personDTO.getUserName());
		person.setCreateDate(personDTO.getCreateDate());
		person.setModificationDate(personDTO.getModificationDate());
		person.setStatus(personDTO.getStatus());
		return person;
	}
}
