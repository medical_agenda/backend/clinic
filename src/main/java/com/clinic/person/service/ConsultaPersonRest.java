package com.clinic.person.service;

import java.util.List;

import com.clinic.person.module.Person;

import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@ApplicationScoped
public class ConsultaPersonRest {
	@PersistenceContext
	EntityManager em;

	public List<Object> findAll() {
		return em.createQuery("SELECT p FROM Person p ").getResultList();
	}
}
