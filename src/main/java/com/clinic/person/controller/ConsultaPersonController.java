package com.clinic.person.controller;

import java.util.List;

import com.clinic.person.service.ConsultaPersonRest;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

//http://localhost:8080/clinic/service/ConsultaPersona/consultarPersona
@Path("/ConsultaPersona")
public class ConsultaPersonController {
	@Inject
	ConsultaPersonRest consultaPersonaRest = new ConsultaPersonRest();

	@GET
	@Path("/consultarPersona")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Object> findAll() {
		return consultaPersonaRest.findAll();
	}
}
