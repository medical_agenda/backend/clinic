package com.clinic.person.controller;

import com.clinic.person.module.Person;
import com.clinic.person.module.PersonDTO;
import com.clinic.person.service.GestionPersonRest;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

//http://localhost:8080/clinic/service/GestionPersona/gestionPersona
@Path("/GestionPersona")
@Stateless
public class GestionPersonController {
	@Inject
	GestionPersonRest gestionPersonRest = new GestionPersonRest();

	@POST
	@Path("/save")
	@Produces(MediaType.APPLICATION_JSON)
	public void save(PersonDTO personDTO) {
		gestionPersonRest.save(personDTO);
	}

	@PUT
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	public void update(PersonDTO personDTO) {
		gestionPersonRest.update(personDTO);
	}

	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Person search(@QueryParam("id") Long idPerson) {
		return gestionPersonRest.search(idPerson);
	}

	@DELETE
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Boolean delete(@QueryParam("id") Long idPerson) {
		return gestionPersonRest.delete(idPerson);
	}
}
