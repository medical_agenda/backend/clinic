package com.clinic.person.module;

import java.time.LocalDate;

public class PersonDTO {
	private	Long			id;
	private	String			identificationNumber; 
	private	String			firstName;
	private	String			secondName;
	private	String			fistLastName;
	private	String			secondLastName; 
	private	String			phoneNumber; 
	//private	PersonTypeEnum	personTypeEnum; 
	private	Long			dTypId;
	private	String			email;
	private	LocalDate		birthDate;
	private	String			userName;
	private	LocalDate		createDate; 
	private	LocalDate		modificationDate;
	private	Boolean			status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIdentificationNumber() {
		return identificationNumber;
	}
	
	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public String getFistLastName() {
		return fistLastName;
	}
	
	public void setFistLastName(String fistLastName) {
		this.fistLastName = fistLastName;
	}
	public String getSecondLastName() {
		return secondLastName;
	}
	
	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/*
	public PersonTypeEnum getPersonTypeEnum() {
		return personTypeEnum;
	}
	public void setPersonTypeEnum(PersonTypeEnum personTypeEnum) {
		this.personTypeEnum = personTypeEnum;
	}*/
	
	public Long getDTypId() {
		return dTypId;
	}
	public void setDTypId(Long dTypId) {
		this.dTypId = dTypId;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public LocalDate getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public LocalDate getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDate createDate) {
		this.createDate = createDate;
	}
	
	public LocalDate getModificationDate() {
		return modificationDate;
	}
	public void getModificationDate(LocalDate modificationDate) {
		this.modificationDate = modificationDate;
	}
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
}
